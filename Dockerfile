FROM ubuntu:16.04
MAINTAINER avendretter

#UPDATE AND INSTALL SECTION
RUN dpkg --add-architecture i386 && \
	apt-get update && \
	apt-get install -y mailutils \
					postfix \
					curl \
					wget \
					file \
					bzip2 \
					gzip \
					unzip \
					bsdmainutils \
					python \
					util-linux \
					ca-certificates \
					binutils \
					bc \
					jq \
					tmux \
					lib32gcc1 \
					libstdc++6 \
					libstdc++6:i386 

#USER CREATION
RUN useradd -d /home/core -ms /bin/bash -G root,sudo -p core core
USER core

#CORE INSTALLATION
RUN wget -O /home/core/linuxgsm.sh https://linuxgsm.sh && \
	chmod +x /home/core/linuxgsm.sh

USER root